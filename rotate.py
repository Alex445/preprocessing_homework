# %%
import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
# %%
# , cv2.COLOR_BGR2GRAY
img_GRAY = cv2.imread("GRAY.JPG", cv2.COLOR_BGR2GRAY)
img_RGB_half = cv2.imread("RGB_half.JPG", cv2.COLOR_BGR2RGB)
img_RGB_quater = cv2.imread("RGB_quater.JPG", cv2.COLOR_BGR2RGB)


# %%
plt.imshow(img_GRAY, cmap='gray')
plt.title('GRAY')
plt.show()

plt.imshow(img_RGB_half)
plt.title('RGB_half')
plt.show()

plt.imshow(img_RGB_quater)
plt.title('RGB_quater')
plt.show()


# %%
def rotate_image(rotated, original):
    rotated_copy, original_copy = rotated.copy(), original.copy()
    fx, fy = rotated.shape[1] / \
        original.shape[1], rotated.shape[0] / original.shape[0]

    minHessian = 400
    detector = cv2.xfeatures2d_SURF.create(hessianThreshold=minHessian)
    keypoints_obj, descriptors_obj = detector.detectAndCompute(original, None)
    keypoints_scene, descriptors_scene = detector.detectAndCompute(
        rotated, None)

    matcher = cv2.DescriptorMatcher_create(cv2.DescriptorMatcher_FLANNBASED)
    knn_matches = matcher.knnMatch(descriptors_obj, descriptors_scene, 2)

    ratio_thresh = 0.75
    good_matches = []
    for m, n in knn_matches:
        if m.distance < ratio_thresh * n.distance:
            good_matches.append(m)

    img_matches = np.empty((max(original_copy.shape[0], rotated_copy.shape[0]),
                            original_copy.shape[1]+rotated_copy.shape[1], 3), dtype=np.uint8)
    cv2.drawMatches(original_copy, keypoints_obj, rotated_copy, keypoints_scene,
                    good_matches, img_matches, flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

    obj = np.empty((len(good_matches), 2), dtype=np.float32)
    scene = np.empty((len(good_matches), 2), dtype=np.float32)
    for i in range(len(good_matches)):
        # Get the keypoints from the good matches
        obj[i, 0] = keypoints_obj[good_matches[i].queryIdx].pt[0]
        obj[i, 1] = keypoints_obj[good_matches[i].queryIdx].pt[1]
        scene[i, 0] = keypoints_scene[good_matches[i].trainIdx].pt[0]
        scene[i, 1] = keypoints_scene[good_matches[i].trainIdx].pt[1]

    H, _ = cv2.findHomography(obj, scene, cv2.RANSAC)

    # -- Get the corners from the image_1 ( the object to be "detected" )
    obj_corners = np.empty((4, 1, 2), dtype=np.float32)
    obj_corners[0, 0, 0] = 0
    obj_corners[0, 0, 1] = 0
    obj_corners[1, 0, 0] = original_copy.shape[1]
    obj_corners[1, 0, 1] = 0
    obj_corners[2, 0, 0] = original_copy.shape[1]
    obj_corners[2, 0, 1] = original_copy.shape[0]
    obj_corners[3, 0, 0] = 0
    obj_corners[3, 0, 1] = original_copy.shape[0]

    scene_corners = cv2.perspectiveTransform(obj_corners, H)

    # Draw lines between the corners (the mapped object in the scene - image_2 )
    cv2.line(img_matches, (int(scene_corners[0, 0, 0] + original_copy.shape[1]), int(scene_corners[0, 0, 1])),
             (int(scene_corners[1, 0, 0] + original_copy.shape[1]), int(scene_corners[1, 0, 1])), (0, 255, 0), 4)
    cv2.line(img_matches, (int(scene_corners[1, 0, 0] + original_copy.shape[1]), int(scene_corners[1, 0, 1])),
             (int(scene_corners[2, 0, 0] + original_copy.shape[1]), int(scene_corners[2, 0, 1])), (0, 255, 0), 4)
    cv2.line(img_matches, (int(scene_corners[2, 0, 0] + original_copy.shape[1]), int(scene_corners[2, 0, 1])),
             (int(scene_corners[3, 0, 0] + original_copy.shape[1]), int(scene_corners[3, 0, 1])), (0, 255, 0), 4)
    cv2.line(img_matches, (int(scene_corners[3, 0, 0] + original_copy.shape[1]), int(scene_corners[3, 0, 1])),
             (int(scene_corners[0, 0, 0] + original_copy.shape[1]), int(scene_corners[0, 0, 1])), (0, 255, 0), 4)

    x1_, y1_, x2_, y2_ = int(scene_corners[3, 0, 0] + original_copy.shape[1]), int(
        scene_corners[3, 0, 1]), int(scene_corners[0, 0, 0] + original_copy.shape[1]), int(scene_corners[0, 0, 1])
    x1__, y1__, x2__, y2__ = 100, 0, 100, 600
    l1, l2, m1, m2 = x2_ - x1_, x2__ - x1__, y2_ - y1_, y2__ - y1__
    angle = math.acos(abs((l1*l2 + m1*m2)) / math.sqrt(l1 *
                                                  l1+m1*m1) / math.sqrt(l2*l2+m2*m2))*180/math.pi
    print('angle = ', angle)
    
    plt.imshow(img_matches)
    plt.title('Good Matches & Object detection')
    plt.show()

    rows, cols = rotated_copy.shape[0], rotated_copy.shape[1]
    M = cv2.getRotationMatrix2D((cols/2, rows/2), angle, 1)
    img = cv2.warpAffine(rotated_copy, M, (cols, rows))
    plt.title('img_rotated')
    plt.imshow(img)
    plt.show()
    return img

# %%


def upsize(img, desire_shape):
    actual_shape = img.shape[0], img.shape[1]

    fx, fy = desire_shape[1] / \
        actual_shape[1], desire_shape[0] / actual_shape[0]

    return cv2.resize(img, None, fx=fx, fy=fy, interpolation=cv2.INTER_CUBIC)
# %%


img_half = rotate_image(img_RGB_half, img_GRAY)
img_half = upsize(img_half, img_GRAY.shape)

img_quater = rotate_image(img_RGB_quater, img_GRAY)
img_quater = upsize(img_quater, img_GRAY.shape)
# %%
plt.title('half_rotated')
plt.imshow(img_half)
plt.show()

plt.title('img_quater')
plt.imshow(img_quater)
plt.show()
# %%
def fill_mask(gray, color):
    rgb_gray = cv2.cvtColor(gray, cv2.COLOR_GRAY2RGB)
    
    for i, row in enumerate(color):
        for j, col in enumerate(row):
            if sum(col) == 0 or (sum(col) <= 255*3 and sum(col) >= 228*3):
                color[i, j] = rgb_gray[i, j]
    plt.imshow(color)
    plt.title("GRAY.JPG and RG_quater.JPG")
    plt.show()
    return color
# %%

img_GRAY_IMG_half = fill_mask(img_GRAY, img_half)
img_GRAY_IMG_quater = fill_mask(img_GRAY, img_quater)

# %%

cv2.imwrite('img_GRAY + IMG_half.jpg', img_GRAY_IMG_half)
cv2.imwrite('img_GRAY + IMG_quater.jpg', img_GRAY_IMG_quater)
# %%
